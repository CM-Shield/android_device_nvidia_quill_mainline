#
# Copyright (C) 2023 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include device/nvidia/quill/BoardConfig.mk

TARGET_KERNEL_CLANG_COMPILE   := true
TARGET_KERNEL_RECOVERY_CONFIG :=
BOARD_KERNEL_CMDLINE          := 8250.nr_uarts=1
TARGET_KERNEL_CONFIG          := \
    gki_defconfig \
    tegra_gki.config \
    vendor/t186.config \
    vendor/quill.config
include device/nvidia/quill_mainline/modules.mk
