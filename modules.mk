#
# Copyright (C) 2023 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Xudc
BOARD_VENDOR_KERNEL_MODULES_LOAD := \
    tegra_xudc

# GPU driver
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
    tegra_drm \
    nouveau

# BCM4354
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
     brcmfmac \
     hci_uart

# Realtek ethernet
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
    r8152 \
    r8169

# Fan / Thermal
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
    pwm_tegra \
    pwm_fan \
    tegra_soctherm \
    tegra_bpmp_thermal

# Tegra hdmi audio
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
    snd_hda_codec_hdmi \
    snd_hda_tegra

# Tegra audio processing engine
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
    tegra210_adma \
    tegra_aconnect \
    snd_soc_tegra210_ahub \
    snd_soc_tegra210_admaif \
    snd_soc_tegra210_sfc \
    snd_soc_tegra210_i2s \
    snd_soc_tegra210_mixer \
    snd_soc_tegra210_adx \
    snd_soc_tegra210_amx \
    snd_soc_tegra210_dmic \
    snd_soc_tegra210_mvc \
    snd_soc_tegra210_ope \
    snd_soc_tegra_audio_graph_card

# Nvidia Controllers
BOARD_VENDOR_KERNEL_MODULES_LOAD += \
    hid_nvidia_blake \
    hid_nvidia_shield


# Copy to boot
BOOT_KERNEL_MODULES := \
    qcom-scm.ko \
    arm_smmu.ko \
    tegra-tcu.ko \
    tegra186-cpufreq.ko \
    tegra186-gpc-dma.ko \
    i2c-tegra.ko \
    rtc-tegra.ko \
    max77620.ko \
    gpio-max77620.ko \
    pinctrl-max77620.ko \
    max77620-regulator.ko \
    gpio-pca953x.ko \
    cqhci.ko \
    sdhci-tegra.ko

# Load in first stage boot
BOARD_VENDOR_RAMDISK_KERNEL_MODULES_LOAD := \
    arm_smmu \
    tegra_tcu \
    tegra186_cpufreq \
    tegra186_gpc_dma \
    i2c_tegra \
    rtc_tegra \
    max77620 \
    gpio_max77620 \
    pinctrl_max77620 \
    max77620_regulator \
    gpio_pca953x \
    sdhci_tegra


# Copy to recovery
RECOVERY_KERNEL_MODULES := \
    $(BOOT_KERNEL_MODULES) \
    hid-nvidia-blake.ko \
    hid-nvidia-shield.ko \
    host1x.ko \
    drm_display_helper.ko \
    drm_dp_aux_bus.ko \
    tegra-drm.ko \
    xhci-tegra.ko \
    tegra-xudc.ko

# Load in recovery
BOARD_RECOVERY_RAMDISK_KERNEL_MODULES_LOAD := \
    $(BOARD_VENDOR_RAMDISK_KERNEL_MODULES_LOAD) \
    xhci_tegra \
    tegra_xudc \
    hid_nvidia_blake \
    hid_nvidia_shield \
    tegra_drm
