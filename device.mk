TARGET_TEGRA_AUDIO    := tinyhal
TARGET_TEGRA_CAMERA   :=
TARGET_TEGRA_CEC      :=
TARGET_TEGRA_KERNEL   := 6.1
TARGET_TEGRA_GPU      := drm
TARGET_TEGRA_KEYSTORE := software
TARGET_TEGRA_OMX      := software
TARGET_TEGRA_PBC      :=
TARGET_TEGRA_PHS      :=
TARGET_TEGRA_WIDEVINE :=

TARGET_TEGRA_FIRMWARE_BRANCH := linux-firmware

$(call inherit-product, device/nvidia/quill/device.mk)

# Fingerprint override
BUILD_FINGERPRINT := NVIDIA/quill/quill:9/PPR1.180610.011/4199437_1739.5219:user/release-keys
